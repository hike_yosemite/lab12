#include <stdio.h>
#include <stdlib.h>
#include <libsocket/libinetsocket.h>
#include <ctype.h>
#include <string.h>
#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60


FILE * connect_to_server();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);


int main()
{
    // Connect
    FILE * s = connect_to_server();
    char line[1000];
    fgets(line, 1000, s);
    while(1)
    {
    
    // Menu
    menu();
    
    // Get choice
    char choice = get_choice();
    
    // Handle choice
    switch(choice)
    {
        case 'l':
        case 'L':
            list_files(s);
            break;
        
        case 'd':
        case 'D':
            download(s);
            break;
            
        case 'q':
        case 'Q':
            quit(s);
            exit(0);
            break;
        case 'a':
        case 'A':
            exit(0);
            break;
            
        default:
            printf("Choice must be d, a, l, or q\n");
    }
    }
    
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server()
{
    int socknum = create_inet_stream_socket("runwire.com", "1234", LIBSOCKET_IPv4, 0);
    if(socknum == -1)
    {
        fprintf(stderr, "Could not conncet\n");
        exit(1);
    }
    FILE *fp = fdopen(socknum, "r+");
    if (!fp)
    {
        fprintf(stderr, "Could not convert socket number\n");
        exit(2);
    }
    return fp;
}

/*
 * Display menu of choices.
 */
void menu()
{
    printf("List files\n");
    printf("Download a file\n");
    printf("Quit\n");
    printf("Download all\n");
    printf("\n");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
    printf("Your choice: ");
    char choices[100];
    fgets(choices, 100, stdin);
    return choices[0];
}

void list_files(FILE *s)
{
    fprintf(s, "LIST\n");
    char line[1000];
    fgets(line, 1000, s);
    char filename[100];
    unsigned long size;
    
    printf("%-10s%-20s\n", "Size", "Filename");
    printf("%-10s%-20s\n", "########", "#########");
    
    while(fgets(line, 100, s) != NULL)
    {
        if (strlen(line) < 3) break;
        
        line[strlen(line)] ='\0';
        if (sscanf(line,"%lu %s", &size, filename) > 0)
        {
            printf("%-10lu%-20s\n", size , filename);
        }
    }
}
// Progress bar
void printProgress(double percentage)
{
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}


void download(FILE *s)
{
    char line[1000];
    char filename[100];
    char str[100];
    int size;
    
    printf("Enter the filename: ");
    scanf("%s%*c", filename); //Very important! it gets rid of the "enter" so we can cue the menu without exiting the program. 
    fprintf(s, "SIZE %s\n", filename);
    fgets(line, 1000, s);
    
    char msg[100];
    sscanf(line, "%s%d", msg, &size);
    printf("%s", line);
    printf("%d\n", size);
    
    if(line[0] == '+')
    {
        fprintf(s, "GET %s\n", filename);
        fgets(str, sizeof(str), s);
        printf("string is: %s\n", str);
        FILE *dFile = fopen(filename, "wb+");
        if(!dFile)
        {
            printf("File %s cant be opened for writing\n", filename);
            exit(1);
        }
        unsigned char data[100];
        int so_far = 0;
        int got;
        int bar = 0;
        int remainder; 
        while(so_far != size)
        {
            if (size - so_far > 100) remainder = 100;
            else remainder = size - so_far;
            got = fread(data , sizeof(unsigned char), remainder, s);
            fwrite(data, sizeof(unsigned char), got, dFile);
            so_far += got;
            
            bar = size / 10;
            //printProgress(bar);
        }
         fclose(dFile); //Need this to close the file after writing.
    }
}


 //Close the connection to the server.
void quit(FILE *s)
{
    fclose(s);
}